use actix_web::{get, post, web, App, HttpResponse, HttpServer, Responder};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
struct UserInfo {
    name: String,
    age: u8,
    major:String,
}

#[get("/")]
async fn hello() -> impl Responder {
    HttpResponse::Ok().body("Welcome to this simple Actic Web Service!")
}

#[get("/greet")]
async fn greet(query: web::Query<UserInfo>) -> impl Responder {
    let greeting = format!("Hello, {}! You are {} years old and major in {}.", query.name, query.age, query.major);
    HttpResponse::Ok().body(greeting)
}

#[post("/echo")]
async fn json_echo(json: web::Json<UserInfo>) -> impl Responder {
    HttpResponse::Ok().json(json.into_inner())
}


#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .service(hello)
            .service(greet)
            .service(json_echo)
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}
