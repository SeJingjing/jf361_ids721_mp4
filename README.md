# jf361_ids721_mp4

## Process
Prerequisites:
Rust and Docker installed on your machine

Step 1: Create a new Cargo project in terminal and change into the new directory
```shell
cargo new <project-name>
cd <project-name>
```
Step 2: Add dependencies in `Cargo.toml` and implement the web service function in `src/main.rs`

Step 3: Create the `Dockerfile` in the root of the project

Step 4: Run below commands in terminal
```shell
cargo build
cargo run
```
Step 5: Make sure the `Docker Desktop` is open. Then run below commands in terminal
```shell
docker build -t <image-name> .
docker run -p 8080:8080 <image-name>
```
Then, you can now access the Actix web app at http://localhost:8080.

## Screenshot
Container
<p align="center">
  <img src="container.png" />
</p>

http://localhost:8080
<p align="center">
  <img src="main.png" />
</p>

http://localhost:8080/greet?name=Jingjing&age=22&major=ECE
<p align="center">
  <img src="greet.png" />
</p>

Run `curl -X POST -H "Content-Type: application/json" -d '{"name":"Jingjing","age":22,"major":"ECE"}' http://localhost:8080/echo` in terminal.
<p align="center">
  <img src="echo.png" />
</p>
